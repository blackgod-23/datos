#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Eliminar el caractér c de  un texto
 * 
 * Esta función busca el caractér a eliminar en el texto desde la
 * posición texto[0] e imprime los demás caracteres hasta que llegue a
 * encontrarse un '\0'.
*/

void eliminar(char c){
	
	int i = 0;
	char* texto = "El texto que yo quiera";
	char caracter = texto[0];
	printf("El resultado es: \n");
	
	while(caracter != '\0'){
		
		caracter = texto[i];
		if(caracter != c){
			
			printf("%c", caracter);
			
			}
		i++;	
		}
	printf("\n");
			
		
	
	}
	
/*Invertir un texto 
 * La función invertir recibe un texto, en <i> se guarda la longitud, la
 * variable <caracter> se va a imprimir caractér por caractér de manera
 * que se logre ver el texto invertido, hasta que termine el ciclo.
*/

void invertir(char* texto){
	
	int largo = strlen(texto);
	char* str = calloc(largo + 1, sizeof(char));
	strcpy(str, texto);
	
	int i = strlen(str);
    char caracter;
    
    printf("El resultado es: \n");
    
	while(i != -1){
		
		caracter = 	str[i];
		printf("%c", caracter);
		i--;
		
		}
	printf("\n");
	}

/*Copiar un texto literal a un texto en el heap
 * La función copiar recibe un texto y esta lo copia en el heap con el
 * <strcpy>
*/
	
void copiar(char* texto){
	
	int largo = strlen(texto);
	char* str = calloc(largo + 1, sizeof(char));
	strcpy(str, texto);
	printf("%s", str);
	printf("\n");
	}

int main(){
	
	eliminar('x');
	invertir("Nuwidra");
	copiar("Sopa de gallo");
	
	}
