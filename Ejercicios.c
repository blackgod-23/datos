/* 
 * Archivo de ejercicios
 * Autor: Bryan Gómez Barrantes
*/

#include <stdio.h>

/* Determinar si un número es par o impar
 * Esta función recibe un número del usuario y este lo divide entre 2
 * de forma entera
*/

int espar(int numero){ // Consulta, porqué afecta el ponerle "int" o "char"?
	
	if (numero % 2 == 0){
		printf("\nEl número %d es par \n", numero);
	}
	else{
		printf("\nEl número %d es impar \n", numero);
	}
	return (0);
}

/* Fibonacci
 * Esta función recibira la cantidad de elementos digitada por el usuario
 * e imprime la variable <a> mientras <i> se cumpla
 * 
*/

int fibonacci(int cantidad){
	
	int a = 0, b = 1, c = 1;
	
	for (int i = 0; i <= cantidad; i++){
		c = a + b;
		a = b;
		b = c;
		
	    printf ("%d \n", a);
    }
    
    return (0);
}

/* Factorial
 * Esta función recibe la variable <numf> digitada por el usuario
 * y se comienza a multiplicar de 1 hasta <numf> 
*/

int factorial (int numf){
	
	int x = 1, resultado;
	
	for (int i = 1;i <= numf; i++){
		x *= i;                     
		resultado = x;
	}
		
	printf("\nEl factorial de %d es %d \n", numf, resultado);

	return (0);
}

/* Largo de un número usando for
 * Esta función recibe la variable <numfor> digitada por el usuario
 * y se realiza una división entre 10, el <contador> indica la
 * existencia de un dígito en un número
*/

int largof(int numfor){
	
	int contador = 0;
	
	for(int i = 0;i <= numfor ;i++){
		
		numfor /= 10;
		contador++;
		}

    printf("\nEl largo de su número es %d \n", contador);

	return (0);
	
	}

/* Largo de un número usando while
 * Esta función recibe la varibale <numw> digitada por el usuario,
 * la cual será divida entre 10 y <contador> guardará los digitos
 * de esta división.
*/

int largow(int numw){
	
	int contador = 1;
	
	while(numw/10 > 0){
		
		numw /= 10;
		contador++;
		
		}
	
	printf("\nEl largo del numero es %d \n", contador);
	
	return (0);
	
	}

/* Sumatoria de 0 a n usando for
 * Esta funcion recibe la variable <n> digitada por el usuario,
 * y procede a realizar una suma hasta que <i> = <n>.
*/

int sumatoriaf (int n){
	
	int suma = 0;

	for(int i = 1; i <= n; i++){
		
		suma += i;
		}
		
	printf("\nLa sumatoria de 0 a %d es %d \n", n, suma);
	
	return (0);
	}

/* Sumatoria de 0 a n usando while
 * Esta funcion recibe la variable <n2> digitada por el usuario,
 * mientras se cumpla que <i> <= <n> se realiza la suma respectiva.
*/

int sumatoriaw (int n2){
	
	int suma = 0;
	int i = 0;
	while(i <= n2){
		
		suma += i;
		
		}
	
	printf("\nLa sumatoria de 0 a %d es %d \n", n2, suma);
	
	return (0);
	
	}

/* Número invertido
 * Esta función recibe la variable <num3> digitada por el usuario,
 * este pasa por una división modular y el resultado se guarda en 
 * <residuo>, la variable <numInv> se multiplica por 10 y se le suma el 
 * valor que tiene <residuo> para así acomodar el número invertido
 * correctamente
*/

int invertirnum (int num3){
	
	int residuo,numInv = 0;
	
	while(num3 > 0){
		
		residuo = num3 % 10;
		num3 /= 10;
		numInv = numInv * 10 + residuo;
		
		}
	
	printf("\n Su número invertido es %d \n", numInv);
	
	return (0);
	}

int palindromo (int num4){
	
	int residuo,numInv = 0;
	
	for(int i = 1;i < num4;i++){
		
		residuo = num4 % 10;
		num4 /= 10;
		numInv = numInv * 10 + residuo;
	}
	if(num4 == palindromo(num4)){
		printf("\n El número %d es palindromo \n", num4);
		}
	else{
		printf("\n El número %d no es palindromo \n", num4);			
		}

	return (0);
	}


int main(){
	
	int numero;
	printf("Escribe un número para saber si es par o impar: ");
	scanf("%d", &numero);
	espar(numero);
	
	int cantidad;
	printf("\nEscribe la cantidad de elementos para la sucesión de Fibonacci: ");
	scanf("%d", &cantidad);
	fibonacci(cantidad);
	
	int numfib;
	printf("\nEscribe un número para averiguar su factorial: ");
	scanf("%d", &numfib);
	factorial(numfib);
	
	int numfor;
	printf("\nEscribe un número para saber el largo: ");
	scanf("%d", &numfor);
	largof(numfor);
	
	int numw;
	printf("\nEscribe un número para saber el largo: ");
	scanf("%d", &numw);
	largow(numw);
	
	int n;
	printf("\nEscribe un número para realizar la sumatoria: ");
	scanf("%d", &n);
	sumatoriaf(n);
	
	int n2;
	printf("\nEscribe un número para realizar la sumatoria: ");
	scanf("%d", &n2);
	sumatoriaf(n2);
	
	int num3;
	printf("\nEscribe un número para invertirlo: ");
	scanf("%d", &num3);
	invertirnum(num3);
	
	int num4;
	printf("\nEscribe un número para saber si es palindromo: ");
	scanf("%d", &num4);
	palindromo(num4);
}
